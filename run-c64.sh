#!/bin/bash

ROM="../x16-emulator/rom.bin"
SCALE=2

../x16-emulator/x16emu -rom $ROM -scale $SCALE -prg $1 -run

