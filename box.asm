.include "library/preamble.inc"
.include "library/x16.inc"
.include "library/macros.inc"
.include "library/printhex.inc"



; Variables
; VERA Settings
VBLANK_MASK = %00000001
RES80x40 = %01100000
BOX_X1_VAL = $01
BOX_X2_VAL = $02
BOX_Y1_VAL = $00
BOX_Y2_VAL = $02
BOX_COLOR =  $04
BOX_X_LEN_VAL = $02
BOX_Y_LEN_VAL = $10
TEST = BOX_X_LEN_VAL



; Zero Page Addresses
BOX_X1 = $11
BOX_Y1 = $12
BOX_X2 = $13
BOX_Y2 = $14
CHAR_VAL = $15

.include "library/draw.asm"

start:
main:
  jsr clear_screen
  lda #$00
  sta CHAR_VAL
  sta VERA_ctrl ; Select primary VRAM address
  sta VERA_addr_med ; Set primary address med byte to 0
  sta VERA_addr_low ; Set Primary address low byte to 0
  lda #$10
  sta VERA_addr_high ; Set primary address bank to 0, stride to 2
  lda #RES80x40
  sta VERA_L1_config

  ; a = length, x/y = coords
  lda #$01
  ldx #$00
  ldy #$00
  jsr draw_line

  lda #$01
  ldx #$01
  ldy #$00
  jsr draw_line

  lda #$01
  ldx #$05
  ldy #$05
  jsr draw_line

  lda #$01
  ldx #$06
  ldy #$06
  jsr draw_line

;BOX_X = r0
;BOX_Y = r1
;BOX_X_LEN = r2
;BOX_Y_LEN = r3
;COLOR = r4
  lda #$05
  sta r4
  lda #$0F
  sta r0
  sta r1
  lda #$20
  sta r2
  sta r3
  jsr draw_solid_box

  rts



;read_vram:
;  lda VERA_L1_config
;  jsr printhex
