/* All work and no play...
   By: Tim Soderstrom
*/

#import "library/vera.inc"
#import "library/printhex.inc"


// Kernel Routines
/*
.const CHROUT = $ffd2
.const CINT = $ff81
.const KERNEL_ISR = $eb01
.const MONITOR = $ff00
*/

// VERA Addresses
/*
.const VERAREG   = $9f20
.const VERAIEN   = $9f26
.const VERAISR   = $9f27
*/

// Colors
.const SOME_COLOR = $4E

// System Memory Addresses
.const ISR_HANDLER = $0314

// Zero Page Addresses
.const STRING_POSITION = $01      // The current offset of the string
.const VBLANK_SKIP_MAX = $02      // How many VBLANKs to do nothing for
.const VBLANK_SKIP_COUNT = $03    // Count of current VBLANK skip
.const RANDOM = $04
.const SANITY_COUNTER = $05
.const TYPING_MISTAKES_COUNTER = $06  // Number of mistakes for line up to SANITY_COUNTER
.const MAX_MISTAKES = $07             // How many mistakes we can make per line
.const MISTAKE_SKIP_COUNTER = $08     // How many lines to skip before incrementing max mistakes
.const PREVIOUS_ISR_HANDLER = $10

// Constants
.const RANDOM_SEED = $69
.const LOST_SANITY_VALUE = $01          // Value when we have gone insane
.const MAX_ALLOWED_MISTAKES_VALUE = $0A // Max mistakes allowed per line
.const MISTAKE_SKIP_COUNT_VALUE = $05   // Increase mistakes every 5 lines
.const VBLANK_MASK = %00000001          // A mask to make sure we set VERA correctly.
.const VBLANK_SKIPS = $01               // A constant for how many VBLANKs to skip.
.const EXCLAMATION = $21                // PETSCII value for !
.const Z = $5A                          // PETSCII value for Z
// .const RETURN = $0d

:BasicUpstart2(setup)

// Setup some things before we get going
setup:
  // Disable interrupts so we do not get distracted setting up code
  sei


set_color:
  lda #$80
  sta VERACTL //; Select primary VRAM address

  lda #$0F
  sta VERAHI //; Set primary address bank to F
  lda #$20
  sta VERAMID //; Set primary address high byte to 20

  // Set Mode
  lda #$00
  sta VERALO //; Set Primary address low byte to 00
  lda #$20
  sta VERADAT

  // Set Color
  lda #$08
  sta VERALO //; Set Primary address low byte to 03
  lda #$FF
  sta VERADAT

  jsr CINT  // Clear the screen

  // Initialize Variables
  /* We set the VBLANK_SKIP_MAX to the constant we specified in VBLANK_SKIPS.
     This basically adds in a delay so the program "types slower" */
  ldx #VBLANK_SKIPS
  stx VBLANK_SKIP_MAX

  /* Set some counters to zero */
  ldx #$00
  stx VBLANK_SKIP_COUNT
  stx STRING_POSITION
  stx SANITY_COUNTER
  stx TYPING_MISTAKES_COUNTER
  stx MAX_MISTAKES
  stx MISTAKE_SKIP_COUNTER

  /* Set random seed */
  lda #RANDOM_SEED
  sta RANDOM

  // Setup typing irq handler
  /* We load the address of our interrupt handler into a special memory
     location. Basically when an interrupt triggers, this is the
     routine the CPU will execute. */
  ldx #$00
  lda ISR_HANDLER,x
  sta PREVIOUS_ISR_HANDLER,x
  lda #<typing_irq
  sta ISR_HANDLER,x

  inx
  lda ISR_HANDLER,x
  sta PREVIOUS_ISR_HANDLER,x
  lda #>typing_irq
  sta ISR_HANDLER,x

  // Enable VBLANK Interrupt
  /* We will use VBLANK from the VERA as our time keeper, so we enable
     the VBLANK interupt */
  lda #VBLANK_MASK
  sta VERAIEN




// Main loop
loop:
  cli         // Enable interrupts
  jsr lsfr    // Generate some random data
  jmp loop    // Loop while we wait

/* The main typing interrupt handler. This is what does the actual work */
typing_irq:
  // Disable interrupts since we don't want to be interrupted (hah)
  sei

  /* Check to see if the VBLANK was triggered. This is in case the intterupt
     was triggered by something else. We just want VBLANK. */
  lda VERAISR         // Load contents of VERA's ISR
  and #VBLANK_MASK    // Mask first bit.
  clc
  cmp #VBLANK_MASK    // If it's not 1, we blanked, continue
  bcc typing_irq_end  // if it's not 1, return

  // Clear the VBLANK Interrupt Status
  lda VERAISR
  and #VBLANK_MASK
  sta VERAISR

  /* This is an additional delay. We increment a counter on every VBLANK and
     once we reach the max, we continue. Otherwise we jump back to the main
     loop. This is what slows the typing down. */
  inc VBLANK_SKIP_COUNT   // increment delay counter
  ldy VBLANK_SKIP_COUNT   // load delay counter
  cpy VBLANK_SKIP_MAX     // compare delay count to max delay
  bcc typing_irq_end      // if it's not equal, go back to loop

  // We have finished delaying, so we reset the counter and continue.
  ldx #$00
  stx VBLANK_SKIP_COUNT

  /* If we have lost our sanity, add some random
     typable characters */
check_sanity:
  lda #LOST_SANITY_VALUE
  cmp SANITY_COUNTER
  bne print_char

  /* If we are insane, now check to see if we can make typing mistakes */
check_typing_mistakes:
  lda TYPING_MISTAKES_COUNTER
  cmp MAX_MISTAKES
  beq print_char

check_greater_than_exclamation:
  lda RANDOM
  cmp #EXCLAMATION
  bcs check_less_than_Z
  jmp print_char

check_less_than_Z:
  cmp #Z
  bcc random_char
  jmp print_char

random_char:
  jsr CHROUT
  inc TYPING_MISTAKES_COUNTER

print_char:
  /* Finally print a character on the screen and increment the position for
     the next time */
  ldx STRING_POSITION
  lda msg,x
  jsr CHROUT

  inc   STRING_POSITION

  /* If A is not 0, we know we do not need to reset the string so we
     jump to the end since we are done. Otherwise, we reset the string
     postion back to 0 so we can start printing the string all over again. */
  cmp #$00                // Compare A to end of string
  bne typing_irq_end      // If it's not 0, go to end of handler
  ldx #$00                // Otherwise reset string position
  stx STRING_POSITION     // Start over with string pointer
  stx TYPING_MISTAKES_COUNTER

increase_sanity:
  /* Let's also mess with our sanity counter. */
  lda #LOST_SANITY_VALUE
  cmp SANITY_COUNTER
  beq check_max_mistakes
  inc SANITY_COUNTER
  jmp typing_irq_end

check_max_mistakes:
  lda MISTAKE_SKIP_COUNTER
  cmp #MISTAKE_SKIP_COUNT_VALUE
  beq increase_max_mistakes
  inc MISTAKE_SKIP_COUNTER
  jmp typing_irq_end

increase_max_mistakes:
  lda #$00
  sta MISTAKE_SKIP_COUNTER
  lda #MAX_ALLOWED_MISTAKES_VALUE
  cmp MAX_MISTAKES
  beq typing_irq_end
  inc MAX_MISTAKES

typing_irq_end:
  jmp (PREVIOUS_ISR_HANDLER)        // Pass control to the previous handler

lsfr:
        lda RANDOM
        lsr
        bcc lsfr_noeor
        eor #$B4
lsfr_noeor:
        sta RANDOM
        rts

/* Message to display on screen. Since we use a string pointed, it has to be
   less than 255. */
msg:  .text "ALL WORK AND NO PLAY MAKES JACK A DULL BOY. "
      .byte RETURN
      .byte 0
