/* Read from keyboard until user presses return,
   then display the contents.

   Written for use with Kick-Ass. Other assemblers will require some
   modifications.

   Useful Links:
    http://www.obelisk.me.uk/6502/reference.html#STA
    http://www.6502.org/users/andre/petindex/keyboards.html#hints
    https://github.com/commanderx16/x16-docs/
    http://sta.c64.org/cbm64krnfunc.html
    http://www.zimmers.net/cbmpics/cbm/c64/c64prg.txt
*/
:BasicUpstart2(setup)

/* C64/x16 Kernel Routines */
.const CINT = $ff81
.const CLRCHN = $ffcc
.const CHROUT = $ffd2
.const PLOT = $fff0
.const READ_KEYBOARD = $ffe4

/* Keyboard values */
.const ENTER_KEY = $0d

/* Our stuff */
/*
.const STRING_LENGTH  = $8001
.const STRING_ADDRESS = $8002
.const STRING_BUFFER  = $8100
*/

// Use zero page instead of absolute addressing
.const STRING_LENGTH  = $01
.const STRING_ADDRESS = $02
.const STRING_BUFFER  = $10

/* Macros */
// Move the cursor to given X,Y coordinates
.macro GotoCoords (x,y) {
    ldx   #x
    ldy   #y
    clc
    jsr   PLOT
}

// Print string from given block
// Block should be something like:
// msg:    .text  STRING
//         .byte 0
.macro PrintString (msg) {
            ldx   #0
    loop:   lda   msg,x
            jsr   CHROUT
            inx
            cmp   #0
            bne   loop
}

/* Assembly */
setup:          // Some Screen Setup
                jsr   CLRCHN  // Reset I/O
                jsr   CINT    // Clear screen
                GotoCoords(0,0) // Goto 0,0 decimal  on screen

                // Reset registers
                ldx   #$0
                ldy   #$0

main:           PrintString(str_prompt)
                jsr   read_kb
                PrintString(str_result)
                jsr   read_buff

final:          GotoCoords(20,20)
                rts

/* Read keyboard input into a buffer until ENTER/RETURN is pressed.
   Note that we're actively looping waiting for a non-zero value to be
   returned from READ_KEYBOARD (when a key is pressed).
   This includes backspace which can result in some interesting output.
   Also notice we have to load STRING_LENGTH from memory. This is because
   the READ_KEYBOARD routine uses all 3 registers */
read_kb:        ldx   #0
                stx   STRING_LENGTH
kb_loop:        jsr   READ_KEYBOARD
                cmp   $0
                beq   kb_loop
key_press:      ldx   STRING_LENGTH
                sta   STRING_BUFFER,x
                jsr   CHROUT
                inx
                stx   STRING_LENGTH
                cmp   #ENTER_KEY
                bne   kb_loop
                rts

/* Read the keyboard buffer. */
read_buff:      ldx   #0
                ldy   STRING_LENGTH
read_loop:      lda   STRING_BUFFER,x
                jsr   CHROUT
                inx
                cpx   STRING_LENGTH
                bne   read_loop
                rts

/* Strings */
str_prompt:   .text "ENTER VALUE: "
              .byte 0

str_result:  .text "YOU ENTERED: "
             .byte 0
