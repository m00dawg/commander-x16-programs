/* Print value at memory address */

#import "library/printhex.inc"

:BasicUpstart2(main)

main:
  lda $0314
  // jsr MONITOR
  jsr printhex
  lda $0315
  jsr printhex
  rts
