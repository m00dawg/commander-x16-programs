:BasicUpstart2(setup)

.const CHROUT = $ffd2

// Works for values 0 to 19
setup:  lda #$00

main:   cmp #$0a
        bcs shift
        ora #$30
        jsr CHROUT
        rts

shift:  tax
        lda #$31      // "1" in PETSCII
        jsr CHROUT
        txa
        sbc #$9
        jmp main
