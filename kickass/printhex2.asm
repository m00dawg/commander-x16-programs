/* Print an 8-bit hex number using the routines inline (no include files).
   The assembled file in this version is MUCH smaller and I'm not sure why. */

:BasicUpstart2(setup)

.const CHROUT = $ffd2
.const CHAR0 = $30
.const CHARAT = $40
.const CHARCOLON = $3a

// Number to print
setup:
         lda #$f4
         jsr print
         rts

print:   tax
         lsr
         lsr
         lsr
         lsr
         jsr printc
         txa
         and #$0f
printc:  cmp #$0a
         bcs printl
printn:  adc #CHAR0
         jsr CHROUT
         rts
printl:  clc
         sbc #$08
         clc
         adc #CHARAT
         jsr CHROUT
         rts
