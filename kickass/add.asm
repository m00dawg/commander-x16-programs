/* Ask for two numbers, add them, print result */

#import "library/macros.inc"
#import "library/constants.inc"
#import "library/printhex.inc"
#import "library/chartonum.inc"


.const STRING_LENGTH  = $01
.const STRING_ADDRESS = $02
.const STRING_BUFFER  = $50

.const TEMPNUM = $04
.const NUMBER1 = $05
.const NUMBER2 = $06
.const RESULT = $07

#import "library/keyboardinput.inc"

:BasicUpstart2(main)

main:           PrintString(first_prompt)
                jsr getnum
                sta NUMBER1
                PrintString(second_prompt)
                jsr getnum
                sta NUMBER2
                clc
                lda NUMBER1
                adc NUMBER2
                bcs too_big
                sta RESULT
                PrintString(result_prompt)
                lda RESULT
                jsr printhex
                NewLine()
                jmp main

// fin:            rts

too_big:        PrintString(too_big_prompt)
                NewLine()
                jmp main

getnum:         jsr read_kb
                ldx 0
                lda STRING_BUFFER,x   // First Character
                jsr chartonum
                asl
                asl
                asl
                asl
                clc
                sta TEMPNUM
                inx                   // Second Character
                lda STRING_BUFFER,x
                jsr chartonum
                ora TEMPNUM
                rts

/* Strings */
first_prompt:   .text "FIRST 8BIT NUMBER: $"
                .byte 0

second_prompt:  .text "SECOND 8BIT NUMBER: $"
                .byte 0

result_prompt:   .text "RESULT: $"
                .byte 0

too_big_prompt: .text "OVERFLOW!"
                .byte 0
