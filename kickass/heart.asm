#import "library/vera.inc"
#import "library/macros.inc"

.const HEART = $53
.const VBLANK_MASK = %00000001

.const SCROLL_INC  = $01  // Increment for scroll
.const INC_MAX = $02      // Skip X VSYNC's before we scroll
.const INC_COUNT = $03    // Count of current VSYNC skip

:BasicUpstart2(setup)

setup:
  sei

  // Load variables into memory
  ldx #$05
  stx INC_MAX
  ldx #$00
  stx SCROLL_INC
  stx INC_COUNT

  // Setup scroll irq handler
  lda #<scroll_irq
  sta $0314
  lda #>scroll_irq
  sta $0315

  // Enable VBLANK Interrupt
  lda #VBLANK_MASK
  sta VERAIEN

main:
  lda #$00
  sta VERACTL //; Select primary VRAM address
  lda #$20
  sta VERAHI //; Set primary address bank to 0, stride to 2
  lda #$00
  sta VERAMID //; Set primary address high byte to 0
  sta VERALO //; Set Primary address low byte to 0

// Nested Loops to Print Tons O Hearts
heart:
  lda #HEART  //; PETSCII heart <3
  ldx #$00
  ldy #$00
loop1:
loop2:
  sta VERADAT //; Write chracter
  inx
  cpx #$FF
  bne loop2

  iny
  cpy #$10
  bne loop1

scroll_setup:
  lda #$0F
  sta VERAHI //; Set primary address bank to F
  lda #$20
  sta VERAMID //; Set primary address high byte to 20
  lda #$08
  sta VERALO //; Set Primary address low byte to 08

loop:
  cli
  // PrintString(loop_message)
  jmp loop

scroll_irq:
  sei

  // Check to see if the vblank was triggered
  lda VERAISR     // Load contents of VERA's ISR
  and #VBLANK_MASK  // Mask first bit.
  clc
  cmp #VBLANK_MASK  // If it's not 1, we blanked, continue
  bcc loop          // if it's not 1, go back to loop

  // Clear Interrupt Status
  lda VERAISR
  and #VBLANK_MASK
  sta VERAISR

  lda #$0F
  sta VERAHI //; Set primary address bank to F
  lda #$20
  sta VERAMID //; Set primary address high byte to 20
  lda #$08
  sta VERALO //; Set Primary address low byte to 08

  inc INC_COUNT   // increment delay counter
  ldy INC_COUNT   // load delay counter
  clc
  cpy INC_MAX     // compare delay count to max delay
  bcc loop        // if it's not equal, go back to loop

  ldy #$00        // set delay count to 0
  sty INC_COUNT

  ldx SCROLL_INC  // Increment scroll counter
  stx VERADAT     // Store that in the vscroll register
  inc SCROLL_INC  // increment scroll counter

  PrintString(interrupt)
  // cli
  jmp loop

loop_message: .text "LOOPING"
              .byte 0

interrupt:   .text "INTERRUPT"
                .byte 0

interrupt_start:   .text "START"
                .byte 0
