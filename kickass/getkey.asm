/* Reads a key from the keyboard then just outputs it to the screen
   in an infinite loop (so you can just typing whatever you want).
   Not super useful but shows how basic keyboard/screen I/O works. */

:BasicUpstart2(setup)

.const CINT = $ff81
.const CLRCHN = $ffcc
.const CHROUT = $ffd2
.const PLOT = $fff0
.const READ_KEYBOARD = $ffe4

/* Macros */
// Wait for keyboard input


// Move the cursor to given X,Y coordinates
.macro GotoCoords (x,y) {
        ldx     #x
        ldy     #y
        clc
        jsr     PLOT
}

// Print string from given block
// Block should be something like:
// msg:    .text  STRING
//         .byte 0
.macro PrintString (msg) {
            ldx     #0
    loop:   lda   msg,x
            jsr   CHROUT
            inx
            cmp   #0
            bne   loop
}

setup:
        // Some Screen Setup
        jsr     CLRCHN  // Reset I/O
        jsr     CINT    // Clear screen

        // Goto 20,20 decimal  on screen
        GotoCoords(0,0)

        // Reset registers
        ldx     #$0
        ldy     #$0

main:   PrintString(str_value)
        jsr loop

readkb: lda #$0
        jsr READ_KEYBOARD
        rts

loop:   jsr readkb
        jsr CHROUT
        jmp loop

str_value:
        .text "ENTER VALUE: "
        .byte 0
