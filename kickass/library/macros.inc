#import "constants.inc"
/* Macros */
// Move the cursor to given X,Y coordinates
.macro GotoCoords (x,y) {
    ldx   #x
    ldy   #y
    clc
    jsr   PLOT
}

// This could probably be a normal routine over a macro
// Print string from given block
// Block should be something like:
// msg:    .text  STRING
//         .byte 0
.macro PrintString (msg) {
            ldx   #0
    loop:   lda   msg,x
            jsr   CHROUT
            inx
            cmp   #0
            bne   loop
}

// Go to new line (Press return)
.macro NewLine () {
  lda #ENTER_KEY
  jsr CHROUT
}
