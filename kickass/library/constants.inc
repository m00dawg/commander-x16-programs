/* C64/x16 Kernel Routines */
.const CINT = $ff81
.const CLRCHN = $ffcc
.const CHROUT = $ffd2
.const PLOT = $fff0
.const READ_KEYBOARD = $ffe4
.const KERNEL_ISR = $eb01
.const MONITOR = $ff00

/* Keyboard values */
.const ENTER_KEY = $0d
.const RETURN = $0d
.const CHAR0 = $30
.const CHARAT = $40
.const CHARA = $41
.const CHARCOLON = $3a
