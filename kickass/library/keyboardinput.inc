/* Read keyboard input into a buffer until ENTER/RETURN is pressed.
   Note that we're actively looping waiting for a non-zero value to be
   returned from READ_KEYBOARD (when a key is pressed).
   This includes backspace which can result in some interesting output.
   Also notice we have to load STRING_LENGTH from memory. This is because
   the READ_KEYBOARD routine uses all 3 registers */
read_kb:        ldx   #0
                stx   STRING_LENGTH
kb_loop:        jsr   READ_KEYBOARD
                cmp   $0
                beq   kb_loop
key_press:      ldx   STRING_LENGTH
                sta   STRING_BUFFER,x
                jsr   CHROUT
                inx
                stx   STRING_LENGTH
                cmp   #ENTER_KEY
                bne   kb_loop
                rts

/* Read the keyboard buffer. */
read_buff:      ldx   #0
                ldy   STRING_LENGTH
read_loop:      lda   STRING_BUFFER,x
                jsr   CHROUT
                inx
                cpx   STRING_LENGTH
                bne   read_loop
                rts
