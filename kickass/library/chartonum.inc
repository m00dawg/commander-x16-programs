/* Get an 8-bit hex value entered from keyboard */

.const SUBTRACT_FOR_DIGIT = $2F
.const SUBTRACT_FOR_CHAR = $36

// Convert character to it's real (hex) number
chartonum:      cmp #$41
                bcs charlet
charnum:        clc
                sbc #SUBTRACT_FOR_DIGIT
                rts
charlet:        clc
                sbc #SUBTRACT_FOR_CHAR
                rts
