.include "library/preamble.asm"
.include "library/x16.inc"
.include "library/macros.inc"
.include "library/printing/printhex.asm"
.include "library/printing/printhex16.asm"
.include "library/printing/printstring.asm"
.include "library/drawing/clearscreen.asm"
.include "library/drawing/drawcharacters.asm"
.include "library/math/add16.asm"
.include "library/files/loadfiletobank.asm"

; Variables
; VERA Settings
VBLANK_MASK  = %00000001
RES80x40x256 = %01101000
RES80x40x16  = %01100000

; Vars
TITLE_COLORS = $0F
SONG_TITLE_X = $09
SONG_TITLE_Y = $01
AUTHOR_TITLE_X = $09
AUTHOR_TITLE_Y = $02

;MAP0 = $00
;MAP1 = $40 ; This is include, tilebase is at 7C by default
;MAP2 = $80
;MAP3 = $C0

start:
main:
  jsr clear_screen
  lda #$00
  sta VERA_ctrl ; Select primary VRAM address
  sta VERA_addr_med ; Set primary address med byte to 0
  sta VERA_addr_low ; Set Primary address low byte to 0
  lda #$00
  sta VERA_addr_high ; Set primary address bank to 0, stride to 0
  lda #RES80x40x256
  sta VERA_L1_config
  jsr clear_screen

  ; Draw Frame
  ; Note for files, seems like the first 4 bytes of the file are skipped
  lda #<frame_filename
  sta r0
  lda #>frame_filename
  sta r0+1
  lda #$09
  jsr load_file_to_bank
  lda #<RAM_WIN
  sta r0+0
  lda #>RAM_WIN
  sta r0+1
  jsr draw_characters

  ; Display song title
  lda #SONG_TITLE_X    ;x
  sta r1
  lda #SONG_TITLE_Y    ;y
  sta r2
  lda #TITLE_COLORS   ;color
  sta r3
  lda #<song_title_string
  sta r0+0
  lda #>song_title_string
  sta r0+1
  jsr print_string

  ; Display song author
  lda #AUTHOR_TITLE_X    ;x
  sta r1
  lda #AUTHOR_TITLE_Y    ;y
  sta r2
  lda #TITLE_COLORS   ;color
  sta r3
  lda #<author_string
  sta r0+0
  lda #>author_string
  sta r0+1
  jsr print_string

@loop:
  jmp @loop


.segment "DATA"

frame_filename: .byte "frame.hex"
heart_filename: .byte "heart.hex"
song_title_string: .byte "first song 123!",0
author_string: .byte "m00dawg",0

; [x : y : character : color], [x : y; ,...]
file_data: .byte 0
